const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/select2.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/subscribe/sass/subscribe.scss', 'public/css')
   .sass('resources/sass/policy/sass/policy.scss', 'public/css')
   .sass('resources/sass/select2/sass/select2.scss', 'public/css')
   .sass('resources/sass/dont-miss-out/sass/dont-miss-out.scss', 'public/css')
   .sass('resources/sass/cookie-bar/sass/cookie-bar.scss', 'public/css');
